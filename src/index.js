import 'core-js';
import 'custom-event-polyfill';
import 'whatwg-fetch';

import Usercentrics from '@usercentrics/cmp-browser-sdk';

const UC_CONSENT_CODE = '';

(function(global) {

    const UC = new Usercentrics(UC_CONSENT_CODE);
    
})(window);
